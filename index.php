<!doctype html>
<html lang="en">
 <head> 
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
  <meta charset="UTF-8">
  <meta name="Generator" content="EditPlus®">
  <meta name="Author" content="">
  <meta name="Keywords" content="">
  <meta name="Description" content="">
 
  <style type="text/css">
   @import url(http://fonts.googleapis.com/earlyaccess/nanumgothic.css);
   @import url(http://fonts.googleapis.com/earlyaccess/nanumbrushscript.css);
   @import url(http://fonts.googleapis.com/earlyaccess/hanna.css);
   body, input, button, select {font-family: 'Hanna', cursive; font-size:105%;}


   div{width:100%;}
   .dir_name{width:80%;}
   .change_dir{width:20%;background: #334954; color:#ffffff;border:none;}
   p{text-align:center;}
   body{margin:0;}

   .img_container{
   position:relative;
   overflow: hidden;
   }
   .left_arrow, .right_arrow{
   z-index: 20;
   position: fixed;
   background: url(./circlearrows.png);
   width: 72px;
   height: 72px;
   top: 40%;
   opacity:0.5;
   filter:alpha(opacity=50); /* For IE8 and earlier */
   }
   .left_arrow{
   left:0px;
   }
   .left_arrow:hover{
   background-position:-144px 0px;
   opacity:1.0;
   filter:alpha(opacity=100); /* For IE8 and earlier */   
   }
   .left_arrow:active{
   background-position:-72px 0px;
   }
   .right_arrow{
   background-position: 0px -73px;
   right:0px;
   }
   .right_arrow:hover{
   background-position: -144px -73px;
   opacity:1.0;
   filter:alpha(opacity=100); /* For IE8 and earlier */   
   }
   .right_arrow:active{
   background-position: -72px -73px;
   }
   .curr_img1, .curr_img2{
   width:100%;
   }
   .left_half{
   width:200%;
   }
   .right_half{
   width:200%;
   margin-left:-100%;
   }

   .toolbox{   position:fixed;		    
   bottom:0px;
	 width:	100%;
	 left:0%;
}
   .tools{
   color:white;
   float:left;
   z-index: 20;
   width:33.3333%;
   height:50px;
   border-radius:25px;
   background:#334954;
   opacity:0.3;
   text-align:center;
   font-size:300%;
   filter:alpha(opacity=30); /* For IE8 and earlier */		
   }
   .tools:hover{opacity:0.8;}
  </style>
  <script src="jquery-2.0.3.min.js"></script>
  <?php
//	$dir = getcwd();
	function dir_init($dir_temp = "Action_Comics_Annual_001"){
		global $dir,$a;
		$dir = $dir_temp;
		// Sort in ascending order - this is default
		$a = scandir($dir);
		array_shift($a);
		array_shift($a);// ".", ".."을 없애고
//		echo $dir."<br/>";
	}
//	echo "post값:".$_POST['dir']."<br/>";
	if($_POST['dir']) dir_init($_POST['dir']);
	else dir_init();
  ?>

  <script>
	var dir_name = "<?echo $dir;?>";
	var img_list = <?echo json_encode($a);?>;
	var curr_page = 1;
	var max_page = <?echo count($a);?>;
	var img_width, img_height;
	var img_ratio ; //이미지 비율. 2장짜리 한장으로 나눌 때 사용
	var page_selector="<select class='page_num'>";

	for( i=1; i<=max_page; i++) page_selector+="<option value="+i+">["+i+"/"+max_page+"]</option>";
	page_selector+="</select>";
	
	$().ready(function(){
		$(".curr_img1").attr("src","./"+dir_name+"/"+img_list[curr_page-1]);
		$("title,#info > p").text(img_list[curr_page-1] + " ["+curr_page+"/"+max_page+"]");
		
		$("#info > div").html(page_selector);
		$(".dir_name").val(dir_name).attr("selected","selected");
	
	});

	function move_page(IsNext){ //페이지 이동에 쓰이는 함수
		if(IsNext){	
			if(++curr_page > max_page) curr_page=1;}
		else{
			if(--curr_page < 1) curr_page=max_page;}
		$(".curr_img1").attr("src","./"+dir_name+"/"+img_list[curr_page-1]);
		$("title,#info >p").text(img_list[curr_page-1] + " ["+curr_page+"/"+max_page+"]");
		
		window.scrollTo(0,0);
		$(".curr_img1").load(viewType);
		//보기형식에 따라 이미지 조정
	}

	function page_select(){
		curr_page = $(".page_num").val();
		$(".curr_img1").attr("src","./"+dir_name+"/"+img_list[curr_page-1]);
		$("title,#info >p").text(img_list[curr_page-1] + " ["+curr_page+"/"+max_page+"]");

		window.scrollTo(0,0);
		$(".curr_img1").load(viewType);
	}

	function viewType(){//한장보기		
		img_width = $(".curr_img1").width();
		img_height = $(".curr_img1").height();
		img_ratio = img_width > img_height; //너비>높이 이면 true
		var chk = $(".view_one").is(":checked");	//한장보기(왼쪽->오른쪽)
		var chk2 = $(".view_one_r").is(":checked");	//한장보기(오른쪽->왼쪽)
		if(chk && img_ratio) {				
            $(".curr_img1").removeClass("right_half").addClass("left_half");
			//$(".curr_img1").css("width","200%");
			$(".curr_img2").attr("src","./"+dir_name+"/"+img_list[curr_page-1]).removeClass("left_half").addClass("right_half").show();
			}
		else if(chk2 && img_ratio) {
            $(".curr_img1").removeClass("left_half").addClass("right_half");
			//$(".curr_img1").css("width","200%")
			$(".curr_img2").attr("src","./"+dir_name+"/"+img_list[curr_page-1]).removeClass("right_half").addClass("left_half").show();
			}
		else {
			$(".curr_img1").removeClass("left_half right_half");
			//$(".curr_img1").css("width","100%");
			$(".curr_img2").hide().removeAttr("src");
			}
	};
	$().ready(function(){
			$("[name='show_type']").click(viewType);
		    $(".page_num").change(page_select);

	});


	</script>
  <title>이미지뷰어</title>
 </head>
 <body>
	<?php
	  $d = opendir(getcwd());
	  $output = "<div><form action=".$_SERVER['PHP_SELF']." method='post'><select name='dir' class='dir_name'>";

	  while ($file = readdir($d)){
		  if($file != "." && $file != ".." && is_dir($file))
			  $files[] = $file;
	  }
	  sort($files);
	  
	  foreach($files as $file){
		  $output .=  "<option value='".$file."'>".$file."</option>";
	  }
	  $output .= "</select><input class='change_dir' type='submit' value='변경'></form></div>";
	  echo $output;
    closedir($d);
	?>
   <input type="radio" name="show_type" class="view_100" checked>폭맞춤</input>
   <input type="radio" name="show_type" class="view_one">한장보기(왼쪽->오른쪽)</input>
   <input type="radio" name="show_type" class="view_one_r">한장보기(오른쪽->왼쪽)</input>
   <div class="img_container">
    <img class="curr_img1" src='' >
    <img class="curr_img2" src='' >
    <a class="left_arrow" onclick="move_page(false)"></a>
    <a class="right_arrow" onclick="move_page(true)"></a>
   </div>
   
   <div class="toolbox" hidden><!--test-->
    <div class="tools">-</div>
    <div class="tools">*</div>
    <div class="tools">+</div>
   </div>

   <div id="info"><p>page_info_here</p><div></div></div>

 </body>
</html>